package com.example.calculator1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity()
{
    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.resultTextView)
    }
    fun numberClick(clicledView:View)
    {
        if (clicledView is TextView)
        {
            var result = resultTextView.text.toString()
            val number = clicledView.text.toString()

            if (result == "0"){
               result = ""
            }
            resultTextView.text = result + number
        }
    }
    fun operationClick(clicledView: View)
    {
        if (clicledView is TextView)
        {

            var operand = resultTextView.text.toString()
            this.operand = operand.toDouble()
            operation = clicledView.text.toString()
            resultTextView.text = ""

        }
    }
    fun equalsOnClick(clicledView: View)
    {
        val secOperand = resultTextView.text.toString().toDouble()
        when (operation)
        {
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "/" -> resultTextView.text = (operand / secOperand).toString()

        }
    }
}
